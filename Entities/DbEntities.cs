﻿using Microsoft.EntityFrameworkCore;
using Models;

namespace Entities
{
    public class DbEntities : DbContext
    {
        public DbEntities(DbContextOptions<DbEntities> options) : base(options) { }

        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Coin> Coin { get; set; }
    }
}
