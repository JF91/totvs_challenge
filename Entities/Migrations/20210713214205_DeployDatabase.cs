﻿using Entities.Resources;
using Microsoft.EntityFrameworkCore.Migrations;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace Entities.Migrations
{
    public partial class DeployDatabase : Migration
    {
        // RUN 'update-database' in Package Manager Console to Deploy Database
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<double>(type: "float", nullable: false),
                    Paid = table.Column<double>(type: "float", nullable: false),
                    Change = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransactionCoins",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionId = table.Column<double>(type: "int", nullable: false),
                    CoinId = table.Column<double>(type: "int", nullable: false),
                    Count = table.Column<double>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionCoins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Coins",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<double>(type: "float", nullable: false),
                    Type = table.Column<double>(type: "nvarchar(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coins", x => x.Id);
                });

            migrationBuilder.Sql(Queries.CreateProcedure_spGetAllTransactionCoins);
            migrationBuilder.Sql(Queries.CreateProcedure_spAddTransactionCoins);
            migrationBuilder.Sql(Queries.CreateProcedure_spGetAllTransactions);
            migrationBuilder.Sql(Queries.CreateProcedure_spAddTransaction);
            migrationBuilder.Sql(Queries.CreateProcedure_spGetAllCoins);            

            ResourceSet coinsSet = Coins.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in coinsSet)
            {
                migrationBuilder.Sql(string.Format(Queries.QuerySQL_InsertCoin, entry.Value.ToString().Split("|")[0], entry.Value.ToString().Split("|")[1]));
            }
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Coin");
        }
    }
}
