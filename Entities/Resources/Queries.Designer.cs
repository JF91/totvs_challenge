﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entities.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Queries {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Queries() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Entities.Resources.Queries", typeof(Queries).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -- =============================================
        ///-- Author:		João Franco
        ///-- Description:	&lt;Add new transaction&gt;
        ///-- =============================================
        ///
        ///CREATE PROCEDURE [dbo].[spAddTransaction]
        ///	@Price AS FLOAT,
        ///	@Paid AS FLOAT,
        ///	@Change AS FLOAT
        ///
        ///AS
        ///BEGIN
        ///	-- SET NOCOUNT ON added to prevent extra result sets from
        ///	-- interfering with SELECT statements.
        ///	SET NOCOUNT ON;
        ///
        ///    -- Insert statements for procedure here
        ///	INSERT INTO Transactions
        ///	VALUES (@Price, @Paid, @Change);
        ///       [rest of string was truncated]&quot;;.
        /// </summary>
        public static string CreateProcedure_spAddTransaction {
            get {
                return ResourceManager.GetString("CreateProcedure_spAddTransaction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -- =============================================
        ///-- Author:		João Franco
        ///-- Description:	&lt;Add new transaction coins&gt;
        ///-- =============================================
        ///
        ///CREATE PROCEDURE [dbo].[spAddTransactionCoins]
        ///	@TransactionId AS INT,
        ///	@CoinId AS INT,
        ///	@Count AS INT
        ///
        ///AS
        ///BEGIN
        ///	-- SET NOCOUNT ON added to prevent extra result sets from
        ///	-- interfering with SELECT statements.
        ///	SET NOCOUNT ON;
        ///
        ///    -- Insert statements for procedure here
        ///	INSERT INTO TransactionCoins
        ///	VALUES (@TransactionId [rest of string was truncated]&quot;;.
        /// </summary>
        public static string CreateProcedure_spAddTransactionCoins {
            get {
                return ResourceManager.GetString("CreateProcedure_spAddTransactionCoins", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -- =============================================
        ///-- Author:		João Franco
        ///-- Description:	&lt;Get all coins&gt;
        ///-- =============================================
        ///
        ///CREATE PROCEDURE [dbo].[spGetAllCoins]
        ///
        ///AS
        ///BEGIN
        ///	-- SET NOCOUNT ON added to prevent extra result sets from
        ///	-- interfering with SELECT statements.
        ///	SET NOCOUNT ON;
        ///
        ///    -- Insert statements for procedure here
        ///	SELECT * FROM Coins
        ///END.
        /// </summary>
        public static string CreateProcedure_spGetAllCoins {
            get {
                return ResourceManager.GetString("CreateProcedure_spGetAllCoins", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -- =============================================
        ///-- Author:		João Franco
        ///-- Description:	&lt;Get all transaction coins&gt;
        ///-- =============================================
        ///
        ///CREATE PROCEDURE [dbo].[spGetAllTransactionsCoins]
        ///
        ///AS
        ///BEGIN
        ///	-- SET NOCOUNT ON added to prevent extra result sets from
        ///	-- interfering with SELECT statements.
        ///	SET NOCOUNT ON;
        ///
        ///    -- Insert statements for procedure here
        ///	SELECT * FROM TransactionCoins
        ///END.
        /// </summary>
        public static string CreateProcedure_spGetAllTransactionCoins {
            get {
                return ResourceManager.GetString("CreateProcedure_spGetAllTransactionCoins", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -- =============================================
        ///-- Author:		João Franco
        ///-- Description:	&lt;Get all transactions&gt;
        ///-- =============================================
        ///
        ///CREATE PROCEDURE [dbo].[spGetAllTransactions]
        ///
        ///AS
        ///BEGIN
        ///	-- SET NOCOUNT ON added to prevent extra result sets from
        ///	-- interfering with SELECT statements.
        ///	SET NOCOUNT ON;
        ///
        ///    -- Insert statements for procedure here
        ///	SELECT T.Id, T.Price, T.Change, C.Value, C.Type, TC.Count
        ///    FROM Transactions AS T
        ///    INNER JOIN TransactionCoins [rest of string was truncated]&quot;;.
        /// </summary>
        public static string CreateProcedure_spGetAllTransactions {
            get {
                return ResourceManager.GetString("CreateProcedure_spGetAllTransactions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO Coins
        ///VALUES ({0}, &apos;{1}&apos;).
        /// </summary>
        public static string QuerySQL_InsertCoin {
            get {
                return ResourceManager.GetString("QuerySQL_InsertCoin", resourceCulture);
            }
        }
    }
}
