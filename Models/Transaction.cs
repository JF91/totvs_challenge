﻿namespace Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public double Price { get; set; }

        public double Paid { get; set; }

        public double Change { get; set; }
    }
}
