﻿namespace Models
{
    public class Coin
    {
        public int Id { get; set; }

        public double Value { get; set; }

        public string Type { get; set; }
    }
}
