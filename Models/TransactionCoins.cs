﻿namespace Models
{
    public class TransactionCoins
    {
        public int Id { get; set; }
        public int TransactionId { get; set; }
        public int CoinId { get; set; }
        public int Count { get; set; }
    }
}
