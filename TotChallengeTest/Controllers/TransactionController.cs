﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TotChallengeTest.Interfaces;
using TotChallengeTest.Models;
using TotChallengeTest.Services;

namespace TotChallengeTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService transactionService;
        private readonly IConfiguration configuration;

        public TransactionController(ITransactionService transactionService, IConfiguration configuration)
        {
            this.transactionService = transactionService;
            this.configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await transactionService.Get();
            return Ok(result);
        }

        [HttpPost("")]
        [HttpPost("[action]")]
        public async Task<IActionResult> Pay([FromHeader]double Price, [FromHeader]double Paid)
        {
            CoinService coinService = new CoinService(configuration.GetConnectionString("ef"));
            List<Coin> coins = coinService.Get().Result.OrderByDescending(c => c.Value).ToList();
            List<Coin> coinChange = new List<Coin>();

            double numValidation = 0;
            if (!double.TryParse(Price.ToString(), out numValidation)) return BadRequest();
            if (!double.TryParse(Paid.ToString(), out numValidation)) return BadRequest();
            if (((double)Paid < (double)Price)) return Problem(detail: "Not Enought!", title: "Missing: ");

            double changeValue = Math.Round((double)Paid - (double)Price, 2);
            double changeValueCounter = Math.Round((double)Paid - (double)Price, 2);
            foreach (var coin in coins)
            {
                if (coin.Value >= 1)
                {
                    int billcount = (int)(changeValueCounter / coin.Value); // Calculate remaining change value for the current coin option.
                    changeValueCounter -= billcount * coin.Value; // Reduce change value by the current coin option

                    if (billcount > 0) coinChange.Add(coin); // Add elegible coin option as change value to the list of coins given
                }
                else
                {
                    int coinCount = 0;
                    changeValueCounter = Math.Round(changeValueCounter, 2);
                    while (coin.Value <= changeValueCounter)
                    {
                        changeValueCounter -= coin.Value;
                        coinChange.Add(coin);
                        coinCount++;
                    }
                }
            }

            Pay payResponse = new Pay()
            {
                Price = (double)Price,
                Paid = (double)Paid,
                Change = changeValue,
                CoinsGiven = new List<CoinsGiven>()
            };

            coinChange.GroupBy(coin => coin.Value).ToList().ForEach(coinGiven =>
            {
                payResponse.CoinsGiven.Add(new CoinsGiven()
                {
                    Count = coinGiven.Count(),
                    Value = coinGiven.Key,
                    Type = coinGiven.FirstOrDefault().Type
                });
            });

            int transactionId = await transactionService.Pay((double)Price, (double)Paid, changeValue);
            foreach (CoinsGiven coinGiven in payResponse.CoinsGiven)
            {
                await transactionService.RegisterPayment
                (
                    transactionId, 
                    coins.Where(coin => coin.Value == coinGiven.Value).Select(coin => coin.Id).FirstOrDefault(),
                    coinGiven.Count
                );
            };

            return Ok(payResponse);
        }
    }
}
