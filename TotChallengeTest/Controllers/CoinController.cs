﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TotChallengeTest.Interfaces;

namespace TotChallengeTest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CoinController : ControllerBase
    {
        private readonly ICoinService coinService;
        public CoinController(ICoinService coinService) => this.coinService = coinService;

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await coinService.Get();
            return Ok(result);
        }
    }
}
