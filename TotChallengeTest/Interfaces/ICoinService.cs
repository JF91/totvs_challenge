﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotChallengeTest.Interfaces
{
    public interface ICoinService
    {
        Task<IEnumerable<Coin>> Get();
    }
}
