﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotChallengeTest.Interfaces
{
    public interface ITransactionService
    {
        Task<IEnumerable<object>> Get();
        Task<IEnumerable<TransactionCoins>> GetTransactionCoins();
        Task<int> Pay(double Price, double Paid, double Change);
        Task RegisterPayment(int TransactionId, int CoinId, int Count);
    }
}
