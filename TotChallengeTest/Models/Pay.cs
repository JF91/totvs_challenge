﻿using System.Collections.Generic;

namespace TotChallengeTest.Models
{
    public class Pay
    {
        public double Price { get; set; }
        public double Paid { get; set; }
        public double Change { get; set; }
        public List<CoinsGiven> CoinsGiven { get; set; }
    }

    public class CoinsGiven
    {
        public int Count { get; set; }
        public double Value { get; set; }
        public string Type { get; set; }
    }
}
