﻿using Dapper;
using Microsoft.Data.SqlClient;
using Models;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using TotChallengeTest.Interfaces;

namespace TotChallengeTest.Services
{
    public class CoinService : ICoinService
    {
        private readonly string connectionString;
        public CoinService(string connectionString) => this.connectionString = connectionString;

        public async Task<IEnumerable<Coin>> Get()
        {
            IEnumerable<Coin> coins = null;

            using (var dbContext = new SqlConnection(connectionString))
            {
                coins = await dbContext.QueryAsync<Coin>("spGetAllCoins", commandType: CommandType.StoredProcedure);
            }

            return coins;
        }
    }
}
