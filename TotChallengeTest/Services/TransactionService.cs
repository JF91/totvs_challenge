﻿using Dapper;
using Microsoft.Data.SqlClient;
using Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TotChallengeTest.Interfaces;

namespace TotChallengeTest.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly string connectionString;
        public TransactionService(string connectionString) => this.connectionString = connectionString;

        // GET ALL TRANSACTIONS
        public async Task<IEnumerable<object>> Get()
        {
            IEnumerable<object> transactions = null;

            using (var dbContext = new SqlConnection(connectionString))
            {
                transactions = await dbContext.QueryAsync<object>("spGetAllTransactions", commandType: CommandType.StoredProcedure);
            }

            return transactions;
        }

        // GET ALL TRAANSACTION COINS GIVEN
        public async Task<IEnumerable<TransactionCoins>> GetTransactionCoins()
        {
            IEnumerable<TransactionCoins> transactions = null;

            using (var dbContext = new SqlConnection(connectionString))
            {
                transactions = await dbContext.QueryAsync<TransactionCoins>("spGetAllTransactionsCoins", commandType: CommandType.StoredProcedure);
            }

            return transactions;
        }

        // REGISTER TRANSACTION
        public async Task<int> Pay(double Price, double Paid, double Change)
        {
            using (var dbContext = new SqlConnection(connectionString))
            {
                var queryParameters = new DynamicParameters();
                queryParameters.Add("@Price", Price);
                queryParameters.Add("@Paid", Paid);
                queryParameters.Add("@Change", Change);

                var response = await dbContext.QueryAsync<int>("spAddTransaction", queryParameters, commandType: CommandType.StoredProcedure);
                return response.First();
            }
        }

        // REGISTER TRANSACTION COINS GIVEN
        public async Task RegisterPayment(int TransactionId, int CoinId, int Count)
        {
            using (var dbContext = new SqlConnection(connectionString))
            {
                var queryParameters = new DynamicParameters();
                queryParameters.Add("@TransactionId", TransactionId);
                queryParameters.Add("@CoinId", CoinId);
                queryParameters.Add("@Count", Count);

                await dbContext.QueryAsync("spAddTransactionCoins", queryParameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
